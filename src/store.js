import { configureStore } from "@reduxjs/toolkit";

import { productsReducer } from "./redux/product/productsSlice";

import { cardReducer } from "./redux/card/cardSlice";

export default configureStore({
  reducer: {
    products: productsReducer,
    card: cardReducer,
  },
});
