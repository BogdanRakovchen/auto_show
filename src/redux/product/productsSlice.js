import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  products: [],
  descriptionForProduct: [],
  status: "idle",
  error: null,
};
export const fetchPosts = createAsyncThunk("products/getProducts", async () => {
  const response = await axios.get("http://localhost:3000/products");
  return response.data;
});

const productsSlice = createSlice({
  name: "products",
  initialState,
  reducers: {
    // добавляем данные по товару
    descriptionProduct: {
      reducer(state, action) {
        state.descriptionForProduct = action.payload;
      },
    },
  },
  extraReducers(builder) {
    builder
      .addCase(fetchPosts.pending, (state, action) => {
        state.status = "loading";
      })
      .addCase(fetchPosts.fulfilled, (state, action) => {
        state.status = "succeeded";
        // добавляем данные в массив
        state.products = action.payload;
      })
      .addCase(fetchPosts.rejected, (state, action) => {
        state.status = "failed";
        state.error = action.error.message;
      });
  },
});

export const { postAdded, descriptionProduct } = productsSlice.actions;
export const allProducts = (state) => state.products.products;
export const firstFourProducts = (state) => state.products.products.slice(0, 4);
export const secondFourProducts = (state) =>
  state.products.products.slice(4, 8);
export const threeFourProducts = (state) =>
  state.products.products.slice(8, 12);

export const descriptionForProduct = (state) =>
  state.products.descriptionForProduct;

export const productsReducer = productsSlice.reducer;
