// ---------react---------------
import React from "react";
// --------material ui------------
import Box from "@mui/material/Box";
import useMediaQuery from "@mui/material/useMediaQuery";

// --------react-spring------------
import { useSpring, animated } from "react-spring";

const Basic = () => {
  const matches = useMediaQuery("(max-width:600px)");

  const animationTop = useSpring({
    position: "absolute",
    transition: "margin-right, 2s",
    left: "30%",
    from: { left: "120%" },
    width: "50%",
    height: "40px",
    backgroundColor: "blue",
    border: "1px solid blue",
    borderRadius: "10px 100px / 120px",
    color: "white",
    fontSize: "150%",
    fontWeight: "bold",
    textAlign: "center",
  });

  const animationCenter = useSpring({
    position: "absolute",
    marginTop: "50px",
    transition: "margin-right, 2.5s",
    left: "35%",
    from: { left: "120%" },
    width: "50%",
    height: "40px",
    backgroundColor: "blue",
    border: "1px solid blue",
    borderRadius: "10px 100px / 120px",
    color: "white",
    fontSize: "150%",
    fontWeight: "bold",
    textAlign: "center",
  });

  const animationBottom = useSpring({
    position: "absolute",
    marginTop: "100px",
    transition: "margin-right, 3s",
    left: "40%",
    from: { left: "120%" },
    width: "50%",
    height: "40px",
    backgroundColor: "blue",
    border: "1px solid blue",
    borderRadius: "10px 100px / 120px",
    color: "white",
    fontSize: "150%",
    fontWeight: "bold",
    textAlign: "center",
  });

  const animationBottomOpacity = useSpring({
    position: "absolute",
    marginTop: "200px",
    transform: "scale(1.5)",
    opacity: "1",
    from: { transform: "scale(1)", opacity: "0" },
    left: "30%",
    width: "50%",
    height: "40px",
    backgroundColor: "blue",
    border: "1px solid blue",
    borderRadius: "10px 100px / 120px",
    color: "white",
    fontSize: "150%",
    fontWeight: "bold",
    textAlign: "center",
    transition: "all 2s ease-out 3s",
  });

  // если ширина экрана больше 600px показать анимацию, если меньше, то скрыть
  if (!matches) {
    return (
      <Box component="section">
        <Box component="h2">Автосалон Планета-Авто</Box>
        <Box
          component="div"
          sx={{
            marginBottom: "5%",
            marginTop: "5%",
            display: "flex",
            justifyContent: "space-around",
            overflow: "hidden",
          }}
        >
          <Box
            component="img"
            src="https://cdnimg.rg.ru/img/content/141/92/08/New_KIA_Rio_EXT1_1000_d_850.jpg"
            width="50%"
          ></Box>
          <Box component="div" width="50%" position="relative">
            <animated.span style={animationTop}>С нами быстро</animated.span>
            <animated.span style={animationCenter}>
              С нами выгодно
            </animated.span>
            <animated.span style={animationBottom}>
              С нами недорого
            </animated.span>
            <animated.span style={animationBottomOpacity}>
              И интересно
            </animated.span>
          </Box>
        </Box>
      </Box>
    );
  } else {
    return (
      <Box component="section">
        <Box component="h2">Автосалон Планета-Авто</Box>
        <Box
          component="div"
          sx={{
            marginBottom: "5%",
            marginTop: "5%",
            display: "flex",
            justifyContent: "space-around",
            overflow: "hidden",
          }}
        >
          <Box
            component="img"
            src="https://cdnimg.rg.ru/img/content/141/92/08/New_KIA_Rio_EXT1_1000_d_850.jpg"
            width="86%"
          ></Box>
        </Box>
      </Box>
    );
  }
};

export default Basic;
