//-----------react-----------------
import React from "react";
//----------material-ui--------------
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import Stack from "@mui/material/Stack";
//---------react-router-dom-----------
import { NavLink } from "react-router-dom";
//---------localstorage--------------
import { storage } from "../App";
//--------styled-components----------
import styled from "styled-components";

const Header = ({ auth, value1 }) => {
  let activeStyle = {
    fontSize: "20px",
    textDecoration: "none",
    color: "white",
  };

  let styleLinkDefault = {
    textDecoration: "none",
    color: "white",
  };

  const LinkStyle = styled.span`
    &:hover {
      transition: border-bottom 0.2s;
      border-bottom: 1px solid white;
    }
  `;

  const changeStateInLocalstorage = (boolean) => {
    if (boolean === "true") {
      return true;
    } else if (boolean === "false") {
      return false;
    }
  };

  let booleanState = changeStateInLocalstorage(storage.getItem("state"));

  return (
    <>
      <CssBaseline />

      <Box
        className="box"
        sx={{
          bgcolor: "blue",
          height: "100px",
          width: "lg",
          display: "flex",
          alignItems: "center",
        }}
      >
        <Container>
          <Stack
            direction="row"
            spacing={{ xs: 1, sm: 10, md: 45, lg: 80, xl: 75 }}
          >
            <Box component="div"></Box>
            <Box component="nav">
              <Stack
                component="ul"
                direction="row"
                spacing={{ xs: 1, sm: 2, md: 3, lg: 2, xl: 5 }}
              >
                {/* при авторизации становится true и открывает эти ссылки навигации */}

                {booleanState ? (
                  <>
                    <Box component="li" sx={{ listStyle: "none" }}>
                      <LinkStyle>
                        <NavLink
                          to="/"
                          style={({ isActive }) =>
                            isActive ? activeStyle : styleLinkDefault
                          }
                        >
                          Главная
                        </NavLink>
                      </LinkStyle>
                    </Box>

                    <Box component="li" sx={{ listStyle: "none" }}>
                      <LinkStyle>
                        <NavLink
                          to="/about"
                          style={({ isActive }) =>
                            isActive ? activeStyle : styleLinkDefault
                          }
                        >
                          О нас
                        </NavLink>
                      </LinkStyle>
                    </Box>

                    <Box component="li" sx={{ listStyle: "none" }}>
                      <LinkStyle>
                        <NavLink
                          to="/products"
                          style={({ isActive }) =>
                            isActive ? activeStyle : styleLinkDefault
                          }
                        >
                          Товары
                        </NavLink>
                      </LinkStyle>
                    </Box>

                    <Box component="li" sx={{ listStyle: "none" }}>
                      <LinkStyle>
                        <NavLink
                          to="/buybasket"
                          style={({ isActive }) =>
                            isActive ? activeStyle : styleLinkDefault
                          }
                        >
                          Корзина
                        </NavLink>
                      </LinkStyle>
                    </Box>

                    <Box component="li" sx={{ listStyle: "none" }}>
                      <LinkStyle>
                        <NavLink
                          to="/autorization"
                          style={{ color: "white", textDecoration: "none" }}
                          onClick={() => auth(false)}
                        >
                          Выйти
                        </NavLink>
                      </LinkStyle>
                    </Box>

                    {/* при нажатии кнопки "Выйти" становится false и открывает вот эти ссылки навигации */}
                  </>
                ) : (
                  <>
                    <Box component="li" sx={{ listStyle: "none" }}>
                      <LinkStyle>
                        <NavLink
                          to="/registration"
                          style={({ isActive }) =>
                            isActive ? activeStyle : styleLinkDefault
                          }
                        >
                          Регистрация
                        </NavLink>
                      </LinkStyle>
                    </Box>

                    <Box component="li" sx={{ listStyle: "none" }}>
                      <LinkStyle>
                        <NavLink
                          to="/autorization"
                          style={({ isActive }) =>
                            isActive ? activeStyle : styleLinkDefault
                          }
                        >
                          Авторизация
                        </NavLink>
                      </LinkStyle>
                    </Box>
                  </>
                )}
              </Stack>
            </Box>
          </Stack>
        </Container>
      </Box>
    </>
  );
};

export default Header;
