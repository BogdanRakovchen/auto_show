// ------------react----------------
import React from "react";
// -----------material ui---------------
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
// ------------react-router-dom-------------------
import { NavLink, useNavigate } from "react-router-dom";
// ------------localstorage----------------------
import { myStorage } from "./Registration";

function Copyright(props) {
  return <Typography {...props} />;
}

const theme = createTheme();

const Autorization = ({ auth }) => {
  let navigate = useNavigate();

  let userAuth = JSON.parse(myStorage.getItem("user"));

  const handleSubmit = (event) => {
    // запись данных введенные пользователем
    event.preventDefault();
    const data = new FormData(event.currentTarget);
    let emailData = data.get("email");
    let passwordData = data.get("password");

    // проверка на подлинность
    if (userAuth.email === emailData && userAuth.password === passwordData) {
      // если да, то меняем значение колбэка для localstorage в App
      // и переход на главную страницу
      auth(true);
      navigate("/");
    }
  };

  return (
    <>
      <ThemeProvider theme={theme}>
        <Container component="main" maxWidth="xs">
          <CssBaseline />
          <Box
            sx={{
              marginTop: 8,
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <Avatar sx={{ m: 1, bgcolor: "secondary.main" }}></Avatar>
            <Typography component="h1" variant="h5">
              Войти
            </Typography>
            <Box
              component="form"
              onSubmit={handleSubmit}
              noValidate
              sx={{ mt: 1 }}
            >
              <TextField
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
              />

              <Button
                type="submit"
                fullWidth
                variant="contained"
                sx={{ mt: 3, mb: 2 }}
              >
                Войти
              </Button>

              <Grid container>
                <Grid item>
                  <NavLink
                    to="/registration"
                    style={{
                      textDecoration: "none",
                      color: "blue",
                      width: "100%",
                    }}
                    variant="body2"
                  >
                    {"Нет ещё аккаунта? Зарегистрируйтесь"}
                  </NavLink>
                </Grid>
              </Grid>
            </Box>
          </Box>
          <Copyright sx={{ mt: 8, mb: 4 }} />
        </Container>
      </ThemeProvider>
    </>
  );
};

export default Autorization;
