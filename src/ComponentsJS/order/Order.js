//--------------react-----------------
import React, { useState } from "react";
//-------------material-ui------------
import Box from "@mui/material/Box";
import ToggleButton from "@mui/material/ToggleButton";
import Button from "@mui/material/Button";
//-------------redux------------------
import {
  cardDeleted,
  countItemMin,
  countItemAdd,
} from "../../redux/card/cardSlice";
import { useDispatch } from "react-redux";

const Order = ({ title, img, id, price }) => {
  const dispatch = useDispatch();

  let [countState, setCountState] = useState(1);

  const [disabledCounter, setDisabledCounter] = useState(true);

  const deletedCardItem = () => {
    dispatch(cardDeleted(id, title, img, price, countState));
  };

  const countMinItem = () => {
    setCountState(--countState);
    if (countState <= 1) {
      setDisabledCounter(true);
    }
    dispatch(countItemMin(id, price, countState));
  };

  const countAddItem = () => {
    dispatch(countItemAdd(id, price, countState));
    setCountState(++countState);
    if (countState > 1) {
      setDisabledCounter(false);
    }
  };

  return (
    <>
      {/* первый блок слева */}
      <Box
        component="div"
        sx={{
          width: "100%",

          marginTop: "86px",
        }}
      >
        <Box
          component="div"
          sx={{
            width: "70%",
            display: "flex",
            position: "relative",
            background: "#f6f6f6",
            boxShadow: "0 1px 2px rgb(0 0 0 / 18%)",
            borderRadius: "8px",
          }}
        >
          <Box component="div" sx={{ display: "flex", width: "75%" }}>
            <Box
              component="img"
              sx={{ width: "50%", borderRadius: "10px" }}
              src={img}
            ></Box>
            <Box
              sx={{
                marginLeft: "8%",
                paddingTop: "31px",
                fontSize: "1.5rem",
                fontWeight: "bold",
              }}
            >
              {title}

              <Box component="div" sx={{ paddingTop: "20px" }}>
                Цена: {price} руб
              </Box>
            </Box>
          </Box>

          <Box
            component="div"
            sx={{
              width: "10%",
              height: "36px",
              display: "flex",
              justifyContent: "space-around",

              marginTop: "30px",
              marginLeft: "7%",
            }}
          >
            <ToggleButton
              value="web"
              sx={{ marginRight: "10%" }}
              disabled={disabledCounter}
              onClick={countMinItem}
            >
              -
            </ToggleButton>
            <Box component="span" fontSize="25px">
              {countState}
            </Box>
            <ToggleButton
              value="web"
              sx={{ marginLeft: "10%" }}
              onClick={countAddItem}
            >
              +
            </ToggleButton>
          </Box>
          <Button
            onClick={deletedCardItem}
            key={id}
            variant="contained"
            sx={{
              width: "25%",
              height: "25%",
              position: "absolute",
              top: "61%",
              left: "72%",
            }}
          >
            Удалить заказ
          </Button>
        </Box>
      </Box>
    </>
  );
};

export default Order;
